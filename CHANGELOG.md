# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.1.3] - 2024-04-16

### Changed
 * Upgrade the default version of `f5xc-ingress-controller` to tag `v1.1`
 * Bugfix the `f5xc-ingress-controller` not deleting origin pool when k8s service does not exist

## [v0.1.2] - 2023-06-21

### Added
 * Added the proxy configuration for `f5xc-ingress-controller`
 
## [v0.1.1] - 2023-04-11

### Added
 * Documentation for F5-Distributed-Cloud-Ingress-Controller
 
### Changed
 *  Upgrade the default version of `f5xc-ingress-controller` to tag `v1.0`
 *  Improve the `f5xc-ingress-controller` configuration check right after installation

## [v0.1.0] - 2022-12-30

### Added
 *  Helm chart for `f5xc-ingress-controller`
 *  Documentation for the `f5xc-ingress-controller` Helm chart
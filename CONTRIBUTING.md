# Contributing Guidelines

F5XC Ingress Helm project accepts contributions via GitLab pull requests. This document outlines the process to help get your contribution accepted.

## Filing Bugs

If you're experiencing behavior that appears to be a bug in any of the Helm charts, you're welcome to [file an issue](https://gitlab.com/volterra.io/f5xc-ingress-helm/-/issues/new). 

### Reporting a Bug in Helm

This repository is used by Chart developers for maintaining the official charts for Kubernetes Helm. If your issue is in the Helm tool itself, please use the issue tracker in the [helm/helm](https://github.com/helm/helm) repository.

## Contribute to an Existing Chart

You're also welcome to submit ideas for enhancements to our Helm charts. When doing so, please [search the issue list](https://gitlab.com/volterra.io/f5xc-ingress-helm/-/issues) to see if the enhancement has already been filed. If it has, vote for it (add a reaction to it) and optionally add a comment with your perspective on the idea. 

To submit a contribution:

1. [Fork](https://gitlab.com/volterra.io/f5xc-ingress-helm/-/forks/new) the repository and make your changes.

2. Submit a [pull request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

***NOTE***: In order to make testing and merging of PRs easier, please submit changes to multiple charts in separate PRs.


## Code of Conduct

As contributors and maintainers of this project, we pledge to respect all people who contribute through reporting issues, posting feature requests, updating documentation, submitting pull requests or patches, and other activities.

We are committed to making participation in this project a harassment-free experience for everyone, regardless of level of experience, gender, gender identity and expression, sexual orientation, disability, personal appearance, body size, race, ethnicity, age, or religion.

Examples of unacceptable behavior by participants include the use of sexual language or imagery, derogatory comments or personal attacks, trolling, public or private harassment, insults, or other unprofessional conduct.

Project maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct. Project maintainers who do not follow the Code of Conduct may be removed from the project team.

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported by opening an issue or contacting one or more of the project maintainers.

This Code of Conduct is adapted from the Contributor Covenant, version 1.0.0, available at <https://www.contributor-covenant.org/version/1/0/0/code-of-conduct.html>.
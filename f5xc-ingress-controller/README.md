<!--- app-name: F5XC Ingress Controller -->

# F5XC Ingress Controller package

F5XC Ingress Controller is an Ingress controller that manages external access to HTTP services in a Kubernetes cluster using Volterra Platform.

[Overview of F5XC Ingress Controller](https://f5cloud.zendesk.com/hc/en-us/articles/11267474888471-F5-Distributed-Cloud-Ingress-Controller)

## Prerequisites

- Kubernetes 1.19+
- Helm 3.2.0+

## Parameters

NOTE: all parameters are required for the installation.

### Common parameters

| Name              | Description                                               | Value |
| ----------------- | --------------------------------------------------------- | ----- |
| `namespace`       | the namespace that ingress controller will be deployed to | `""`  |


### F5XC Ingress Controller parameters

| Name                                   | Description                                                                                                                                        | Value                              |
| -------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------- |
| `deployment.image`                       | F5XC Ingress Controller image repository                                                                                                            | `gcr.io/volterraio/ves-lb`                        |
| `deployment.tag`                     | F5XC Ingress Controller image tag (immutable tags are recommended)                                                                                                  | `v1.0` |
| `deployment.digest`                            | F5XC Ingress Controller image digest image digest (in the way sha256:aa....)                                                                                 | `sha256:aa...`               |
| `config.url`                     | F5XC Ingress Controller tenant url                                                                                                        | `""`                     |
| `config.siteName`                    | F5XC Ingress Controller site name                                                                                                  | `""`                               |
| `config.tenant`                       | F5XC Ingress Controller tenant ID                                                                                                                 | `""`                               |
| `config.apiCert`                          | F5XC Ingress Controller API Cert in base64 format                                                                                                                       | `""`                               |
| `config.apiKey`                               | F5XC Ingress Controller API Key in base64 format                                                                                                            | `""`                               |
| `existingSecret`                      | Existing secret name with volt-ic-secret credentials (keys: `ApiCert`, `ApiKey`)                                                                                                  | `""`                               |

### Ways to get parameters
To get site sitename / tenant / url:
1. Go to https://[tenant].console.ves.volterra.io/web/workspaces/cloud-and-edge-sites/sites/site_list
2. Choose the desired site and click the dropdown, copy the "tenant" and "name" for the tenant and siteName fields
Note the tenant field requires the full path (different from other places that need tenant), which has the suffix-id
3. The url would be https://[tenant].console.ves.volterra.io/api

To get apiCert and apiKey:
1. Go to https://[tenant].console.ves.volterra.io/web/workspaces/administration/iam/service_credentials to create API Certificate, download and save it to local.
```Permissions needed
      namespace          | role
      -------------------|--------------------
      default            | ves-io-default-role
      all app namespaces | ves-io-admin-role
      shared             | ves-io-admin-role
```
2. Extract the certificate and private key out of the P12 file:
```bash
$ openssl pkcs12 -in ~/[tenant].console.ves.volterra.io.api-creds.p12 -nodes -nokeys -out $HOME/vescred.cert
Enter Import Password:
MAC verified OK
$ openssl pkcs12 -in ~/[tenant].console.ves.volterra.io.api-creds.p12 -nodes -nocerts -out $HOME/vesprivate.key
Enter Import Password:
MAC verified OK
```
3. Then encode it and store it as <BASE64_Value>
```bash
cat $HOME/vescred.cert | base64     --> api_cert
cat $HOME/vesprivate.key | base64   --> api_key
```
The api_key and api_cert should be stored in secret.yaml, but you can also use vault to store them

>**_NOTE:_** When existingSecret is set the previous apiCert and apiKey are ignored.
>
>**_NOTE:_** existingSecret needs to be in the same namespace as deployment

## Configuration and installation details

It is strongly recommended to use immutable tags in a production environment. This ensures your deployment does not change automatically if the same tag is updated with a different image.

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install`. For example,

```bash
$ helm install my-release --set deployment.tag=v1.0 f5xc-ingress-controller
```

The above command sets the `deployment.tag` to `v1.0`.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. For example,

```bash
$ helm install my-release -f values.yaml f5xc-ingress-controller
```

Since the API certificates is valid only for 12 months, the ingress controller needs to update the secret with the new API certificates. After generating the new cert with the steps above, put the new `apiCert` and `apiKey` into `values.yaml` and run the following

```bash
$ helm upgrade -f values.yaml my-release f5xc-ingress-controller
```

The above command will generate a new release with the updated API certificates.

Alternatively, if using the `existingSecret`, after updating the k8s secret in the pod, run the following

```bash
$ helm upgrade --set existingSecret=new-secret my-release f5xc-ingress-controller
```

This command will generate a new release with the updated existing secret.


## License

Copyright &copy; 2022 F5 Inc

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.